from split import *
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model


model = load_model('mnist_cnn_model.keras')
# Test the function
image_path = r"Example_Images/12340.jpg"
digit_images = split_digits(image_path)

# Display the digit images using matplotlib
for i, digit in enumerate(digit_images):
    plt.subplot(1, len(digit_images), i + 1)
    plt.imshow(digit, cmap='gray')
    plt.axis('off')
    print(recognize_digit(digit,model))

plt.show()
print(read_image(image_path))