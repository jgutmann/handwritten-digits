import os
from flask import Flask, render_template, request
from split import read_image
from PIL import Image

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'static/temp/' 

# Default route
@app.route('/')
def main():
    return render_template("index.html")

# Route to evaluate the Picture 
@app.route('/success', methods=['POST'])
def success():
    if request.method == 'POST':
        f = request.files['file']
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], f.filename)

        # Check if the folder exists, if not, create it
        if not os.path.exists(app.config['UPLOAD_FOLDER']):
            os.makedirs(app.config['UPLOAD_FOLDER'])

        # Save the uploaded file temporarily
        temp_path = os.path.join(app.config['UPLOAD_FOLDER'], "temp_" + f.filename)
        f.save(temp_path)

        # Open the image, resize it, and save it to the final destination
        with Image.open(temp_path) as img:
            img.thumbnail((300, 300))
            img.save(file_path)

        # Remove the temporary file
        os.remove(temp_path)
        
        # Recognize the digit from the resized image
        value = read_image(file_path)
        return render_template("result.html", path=file_path, value=value)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')