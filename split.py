import cv2
import numpy as np
from PIL import Image as im
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model

def recognize_digit(digit, model):
    # Resize the digit to 28x28 pixels as required by the MNIST model
    digit_resized = cv2.resize(digit, (28, 28))

    # Normalize the digit to the range [0, 1]
    digit_normalized = digit_resized.astype('float32') / 255.0

    # Reshape the digit to match the input shape of the model (1, 28, 28, 1)
    test_data = digit_normalized.reshape(1, 28, 28, 1)

    # Recognize the digit
    prediction = model.predict(test_data)
    recognized_digit = np.argmax(prediction)

    return recognized_digit

def split_digits(image_path):
    # Load image
    image = cv2.imread(image_path)
    
    if image is None:
        print("Error: Could not load image.")
        return []
    
    # Convert to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # Apply thresholding to get binary image
    _, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    
    # Apply dilation to thicken lines
    kernel = np.ones((3, 3), np.uint8)
    dilated = cv2.dilate(thresh, kernel, iterations=1)
    
    # Find contours
    contours, _ = cv2.findContours(dilated.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    # Sort contours from left to right
    contours = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])
    
    # Iterate through contours and extract digits
    digits = []
    for contour in contours:
        # Get bounding box
        x, y, w, h = cv2.boundingRect(contour)
        
        # Filter out very small contours that might be noise
        if w < 7 or h < 7:
            continue
        
        # Extract digit
        digit = thresh[y:y+h, x:x+w]
        
        # Resize digit to a fixed size
        digit = cv2.resize(digit, (20, 20))

        # Calculate the sum of all pixels 
        average = np.sum(digit)
        
        # Detect a possible 1 and make it a readable 1
        if(average > 60000):
            digit = np.zeros_like(digit)
            digit[2:18, 9:11] = 255
        
        # Pad the digit to make it 28x28
        digit_padded = np.pad(digit, ((4, 4), (4, 4)), mode='constant', constant_values=0)

        # Add digit to list
        digits.append(digit_padded)
    
    return digits

def read_image(image_path):
    # Load the trained model (CNN)
    model = load_model('mnist_cnn_model.keras')

    # Split the image into single digits
    digit_images = split_digits(image_path)

    # Evaluate digits and add them to string (not int, so that leading zeros work)
    result = ""
    for i, digit in enumerate(digit_images):
        result += str(recognize_digit(digit, model))

    return result
