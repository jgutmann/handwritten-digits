
## Handwritten digits

Small project for our Python for Engineers class.

  

## Description

This project is a handwritten digit recognition application developed for a Python for Engineers class. It encompasses both backend and frontend components, utilizing machine learning techniques for digit recognition and providing a rudimentary web interface for user interaction.

  

## Structure

  

### main:
<ul>
	<li>This file contains the Flask application that serves as the backend of the project.</li>
	<li>It defines routes for handling requests, including uploading images, processing them, and displaying results.</li>
	<li>The application saves uploaded images into static/temp/, processes them using functions from split.py, and renders HTML templates to show the results.</li>
</ul>

### split:

<ul>
	<li>provides functions for image preprocessing, digit segmentation, and digit recognition.</li>
	<li>It utilizes OpenCV for image manipulation and TensorFlow/Keras for digit recognition.</li>
	<li>The split_digits function segments digits from an image, while recognize_digit predicts the value of a single digit.</li>
	<li>The read_image function orchestrates the digit recognition process by splitting the image and recognizing individual digits.</li>
</ul>

### train_model:
<ul>
	<li>This script is responsible for training the neural network model used for digit recognition.</li>
	<li>It loads the MNIST dataset, preprocesses the data, builds a CNN model using TensorFlow/Keras, trains the model, and saves it for later use.</li>
	<li>The trained model (mnist_cnn_model.keras) is later loaded by split.py for digit recognition.</li>
</ul>

### visualize:

<ul>
	<li>is a script for testing and visualizing the digit extraction and recognition process.</li>
	<li>It loads the trained model, processes an example image (provided in the Example_Images folder) using the files from the split function, and displays the resulting cropped digits using Matplotlib.</li>
</ul> 

### templates:
<ul>
	<li>Contains two HTML templates for the main file</li>
</ul>

 

## Requirements

The following libraries are required:
<ul> 
	<li>Tensorflow</li>
	<li>OpenCV</li>
	<li>Flask</li>
	<li>Matplotlib</li>
	<li>PIL</li>
</ul>

	pip install tensorflow opencv-python Flask matplotlib Pillow 

## Installation

Just clone the repository, and try it yourself :)
 
 

## Roadmap

There are no further releases planed since we are done with our class.

  

## Support & Contributing

-   For support or inquiries, feel free to open an issue on the GitHub repository or contact the project maintainers.
-   Contributions are welcome! Fork the repository, make your changes, and submit a pull request. We appreciate any improvements or bug fixes to the project.
  
## Authors and acknowledgment

This project was developed as part of a Python for Engineers class by mpiasko, vlaplante, mnovopando and jgutmann. 
